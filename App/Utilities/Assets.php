<?php

namespace App\Utilities;

class Assets{
    public static function get(string $route)
    {
        return $_ENV['BASE_URL'].'assets/'.$route;
    }

    public static function css(string $route)
    {
        return $_ENV['BASE_URL'].'assets/css/'.$route;
    }

    public static function js(string $route)
    {
        return $_ENV['BASE_URL'].'assets/js/'.$route;
    }

    public static function img(string $route)
    {
        return $_ENV['BASE_URL'].'assets/img/'.$route;
    }
}
